'use strict';

var BASE_URL = 'http://localhost:3000/';

describe('Sample app', () => {

    beforeEach(() => {
        goTo(BASE_URL);
    });

    it('should have input box and button', () => {

        expect(input('inputBox').isPresent())
            .toBe(true, 'no element with id inputBox');

        expect(input('addButton').isPresent())
            .toBe(true, 'no element with id addButton');

    });

    it('should have option to add new task', () => {

        var sampleTaskText = new Date().getTime();

        input('inputBox').setValue(sampleTaskText);

        input('addButton').click();

        expect(element(by.tagName('body')).getText()).toContain(sampleTaskText);
    });

    it('should have link to view task details', () => {

        var sampleTaskText = new Date().getTime();

        input('inputBox').setValue(sampleTaskText);
        input('addButton').click();

        linkContainingText(sampleTaskText).click();

        expect(currentUrl()).toContain(getUrl('#/details/'));

        expect(element(by.tagName('body')).getText()).toContain(sampleTaskText);
    });

    it('should have back link on details page', () => {

        var sampleTaskText = new Date().getTime();

        input('inputBox').setValue(sampleTaskText);
        input('addButton').click();

        linkContainingText(sampleTaskText).click();

        link('back-link').click();

        expect(currentUrl()).toBe(getUrl('#/list'));
    });

});

function link(id) {
    return element(by.id(id));
}

function getUrl(path) {
    return BASE_URL.replace(/\/$/, '') + '/' + path;
}

function currentUrl() {
    return browser.getCurrentUrl();
}


function linkContainingText(searchString) {
    var links = element.all(by.tagName('a'))
        .filter(function (each) {
            return each.getText().then(function (text) {
                return text.indexOf(searchString) >= 0;
            })
        });

    expect(links.count()).toBe(1, 'should find exactly one matching link');

    return links.get(0);
}

function input(id) {
    var input = element(by.id(id));

    return {
        getValue: getValue,
        setValue: setValue,
        isPresent : isPresent,
        click : click
    };

    function isPresent() {
        return input.isPresent();
    }

    function setValue(value) {
        if (value == null || value == undefined) {
            return;
        }

        input.clear().sendKeys(value);
    }

    function getValue() {
        return input.getAttribute('value');
    }

    function click() {
        return input.click();
    }
}

function goTo(addr) {
    browser.get(addr).then(function () {
        browser.manage().window().setSize(1920, 1080);
    });
}
